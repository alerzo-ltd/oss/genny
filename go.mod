module gitlab.com/alerzo-ltd/oss/genny

go 1.16

require (
	github.com/go-git/go-git/v5 v5.4.2
	github.com/mitchellh/mapstructure v1.4.3
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
