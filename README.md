# Genny 🪄

OpenAPI fragment generator.

## Getting started
```bash

# Install prism
npm install -g @stoplight/prism-cli
# OR
yarn global add @stoplight/prism-cli

# Install openapi cli 
npm install -g @stoplight/prism-cli
# OR 
yarn global add @redocly/openapi-cli

go get gitlab.com/alerzo-ltd/oss/genny
genny -f <config.yml>
```

## Config

```yaml
spec:
  paths:
    - /cable/purchase
    - /cable/packages
  output: api/oapi.yaml
```