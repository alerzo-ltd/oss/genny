package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/mitchellh/mapstructure"
	"gopkg.in/yaml.v2"
)

type config struct {
	Repo     string   `yaml:"repo"`
	SpecPath string   `yaml:"specPath"`
	Branch   string   `yaml:"branch"`
	Paths    []string `yaml:"paths"`
	Output   string   `yaml:"output"`
}

// OpenAPISpecification represents a fragment of an openapi specification
type OpenAPISpecification struct {
	Paths map[string]interface{} `yaml:"paths"`
}

func main() {
	var (
		configFile string
		branch     string
		cloneRepo  string
	)

	flag.StringVar(&configFile, "f", "config.yaml", "Path to config file")
	flag.StringVar(&branch, "b", "", "Branch to use")
	flag.StringVar(&cloneRepo, "r", "", "Repository to clone")

	flag.Parse()

	var inGitRepo bool

	c := loadConfig(configFile)

	if cloneRepo == "" {
		cloneRepo = c.Repo
	}

	// CLI -b flag always overwrites the config
	if branch == "" {
		branch = c.Branch
	}

	if c.Output == "" {
		log.Fatal("Output path not set")
	}

	repo, err := git.PlainOpen(".")
	if err == nil {
		inGitRepo = true
		log.Println("Detected you're in a git repository")
	}

	// If no branch flag is specified, use the current git branch
	if branch == "" && inGitRepo {
		log.Println("No branch specified, trying to infer from git...")
		headRef, err := repo.Head()
		// get branch name from HEAD ref
		if err == nil {
			branch = headRef.Name().Short()
			log.Printf("Inferred branch: %s", branch)
		}
	}

	// If there's still no branch, yell!
	if branch == "" {
		log.Fatal("Please specify a branch using the -b flag")
	}

	specClonePath, err := ioutil.TempDir(os.TempDir(), "oapi")
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		log.Printf("Removing temp clone dir: %s", specClonePath)
		if err := os.RemoveAll(specClonePath); err != nil {
			log.Fatal(err)
		}
	}()

	log.Printf("Cloning %s branch of %s...\n", branch, cloneRepo)

	cloneCmd := exec.Command("bash", "-c", fmt.Sprintf("git clone --single-branch --branch  %s %s %s\n", branch, cloneRepo, specClonePath))
	cloneCmd.Stdout = os.Stdout
	cloneCmd.Stderr = os.Stderr

	cloneCmd.Run()

	if err != nil || cloneCmd.ProcessState.ExitCode() != 0 {
		log.Fatal("Failed to clone repository")
	}

	// Load the spec and filter out the routes
	var doc = &OpenAPISpecification{}

	specFile := filepath.Join(specClonePath, c.SpecPath)

	b, err := ioutil.ReadFile(specFile)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(b, &doc)
	if err != nil {
		log.Fatal(err)
	}

	// Try to match paths defined in the config.yaml
	includedSpecPaths := make(map[string]interface{})
	allPaths := make([]string, 0, len(doc.Paths))
	for k := range doc.Paths {
		allPaths = append(allPaths, k)
	}

	for _, p := range c.Paths {
		if m := matchPath(p, allPaths); len(m) > 0 {
			for _, v := range m {
				includedSpecPaths[v] = doc.Paths[v]
			}
		} else {
			// Match a glob or yell if no match
			log.Fatalf("No path found matching '%s'", p)
		}
	}

	// Combine the filtered paths with the rest of the spec
	var o map[string]interface{}
	err = yaml.Unmarshal(b, &o)
	if err != nil {
		log.Fatal(err)
	}

	o["paths"] = includedSpecPaths

	// Remove the security definitions
	_, ok := o["security"]
	if ok {
		delete(o, "security")
	}

	b, err = yaml.Marshal(o)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(specFile, b, 0644)
	if err != nil {
		log.Fatal(err)
	}

	bundleCmd := exec.Command("bash", "-c", fmt.Sprintf("openapi bundle %s -o %s", specFile, c.Output))
	bundleCmd.Stdout = os.Stdout
	bundleCmd.Stderr = os.Stderr

	err = bundleCmd.Run()

	if err != nil || bundleCmd.ProcessState.ExitCode() != 0 {
		log.Fatal("Failed to bundle spec")
	}
}

func isInGitRepo() bool {
	_, err := git.PlainOpen(".")
	if err == nil {
		return true
	}
	return false
}

func matchPath(p string, paths []string) []string {
	var matches = make([]string, 0)
	for _, v := range paths {
		if glob(p, v) {
			matches = append(matches, v)
		}
	}
	return matches
}

func loadConfig(p string) *config {
	var c *config
	b, err := ioutil.ReadFile(p)
	if err != nil {
		log.Fatal(err)
	}
	type container struct {
		config `yaml:"genny"`
	}
	var r map[string]interface{}
	err = yaml.Unmarshal(b, &r)
	if err != nil {
		log.Fatal(err)
	}

	err = mapstructure.Decode(r["genny"], &c)
	if err != nil {
		log.Fatal(err)
	}

	if c == nil {
		log.Fatalf("No configuration found in %s", p)
	}

	return c
}

// GLOB The character which is treated like a glob
const GLOB = "*"

// Glob will test a string pattern, potentially containing globs, against a
// subject string. The result is a simple true/false, determining whether or
// not the glob pattern matched the subject text.
func glob(pattern, subj string) bool {
	// Empty pattern can only match empty subject
	if pattern == "" {
		return subj == pattern
	}

	// If the pattern _is_ a glob, it matches everything
	if pattern == GLOB {
		return true
	}

	parts := strings.Split(pattern, GLOB)

	if len(parts) == 1 {
		// No globs in pattern, so test for equality
		return subj == pattern
	}

	leadingGlob := strings.HasPrefix(pattern, GLOB)
	trailingGlob := strings.HasSuffix(pattern, GLOB)
	end := len(parts) - 1

	// Go over the leading parts and ensure they match.
	for i := 0; i < end; i++ {
		idx := strings.Index(subj, parts[i])

		switch i {
		case 0:
			// Check the first section. Requires special handling.
			if !leadingGlob && idx != 0 {
				return false
			}
		default:
			// Check that the middle parts match.
			if idx < 0 {
				return false
			}
		}

		// Trim evaluated text from subj as we loop over the pattern.
		subj = subj[idx+len(parts[i]):]
	}

	// Reached the last section. Requires special handling.
	return trailingGlob || strings.HasSuffix(subj, parts[end])
}
